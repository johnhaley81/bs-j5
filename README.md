# bs-j5

This project provides bindings to [johnny-five](https://github.com/rwaldron/johnny-five).

## Build
```
npm run build
```

## Build + Watch

```
npm run watch
```


## Editor
If you use `vscode`, Press `Windows + Shift + B` it will build automatically